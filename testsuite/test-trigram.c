#include "config.h"

#include <libcodesearch.h>

#include "code-sparse-set.h"

#define TRIGRAM(abc) ((CodeTrigram) {abc[0], abc[1], abc[2]})

static void
test_trigram_simple (void)
{
  const struct {
    const char *str;
    guint n_trigrams;
    CodeTrigram *trigrams;
  } tests[] = {
    { "abcdef", 4, (CodeTrigram[]) { TRIGRAM("abc"), TRIGRAM("bcd"), TRIGRAM("cde"), TRIGRAM("def") } },
    { "test", 2, (CodeTrigram[]) { TRIGRAM("test"), TRIGRAM("est") } },
  };

  for (guint i = 0; i < G_N_ELEMENTS (tests); i++)
    {
      CodeTrigramIter iter;
      CodeTrigram tri;
      guint j = 0;

      code_trigram_iter_init (&iter, tests[i].str, strlen(tests[i].str));
      while (code_trigram_iter_next (&iter, &tri))
        {
          guint encoded = code_trigram_encode (&tri);
          CodeTrigram dec = code_trigram_decode (encoded);

          g_assert_cmpint (j, <, tests[i].n_trigrams);

          g_assert_cmpint (tests[i].trigrams[j].x, ==, tri.x);
          g_assert_cmpint (tests[i].trigrams[j].y, ==, tri.y);
          g_assert_cmpint (tests[i].trigrams[j].z, ==, tri.z);

          g_assert_cmpint (tri.x, ==, dec.x);
          g_assert_cmpint (tri.y, ==, dec.y);
          g_assert_cmpint (tri.z, ==, dec.z);

          j++;
        }

      g_assert_cmpint (j, ==, tests[i].n_trigrams);
    }
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Code/Trigram/simple", test_trigram_simple);
  return g_test_run ();
}
