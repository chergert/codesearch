#include "config.h"

#include <libcodesearch.h>

#include "code-sparse-set.h"

int
main (int   argc,
      char *argv[])
{
  CodeSparseSet set;

  code_sparse_set_init (&set, 1 << 24);

  for (int i = 1; i < argc; i++)
    {
      GMappedFile *mf;
      CodeTrigramIter iter;
      CodeTrigram trigram;
      GError *error = NULL;
      const char *contents;
      gsize len;

      code_sparse_set_reset (&set);

      if (!(mf = g_mapped_file_new (argv[i], FALSE, &error)))
        g_error ("%s", error->message);

      contents = (const char *)g_mapped_file_get_contents (mf);
      len = g_mapped_file_get_length (mf);

      code_trigram_iter_init (&iter, contents, len);

      while (code_trigram_iter_next (&iter, &trigram))
        {
          guint trigram_id = code_trigram_encode (&trigram);
          code_sparse_set_add (&set, trigram_id);
        }

      g_mapped_file_unref (mf);

      g_print ("%s: %u trigrams\n", argv[i], set.len);
    }

  code_sparse_set_clear (&set);

  return 0;
}
