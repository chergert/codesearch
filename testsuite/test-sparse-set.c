/* test-sparse-set.c
 *
 * Copyright 2022 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <libcodesearch.h>

#include "code-sparse-set.h"

static void
test_sparse_set (void)
{
  const guint capacity = 1<<8;
  CodeSparseSet set;

  code_sparse_set_init (&set, capacity);

  for (int i = capacity; i > 1; i--)
    code_sparse_set_add_with_data (&set, i-1, 1024+(i-1));

  g_assert_false (code_sparse_set_contains (&set, 0));
  g_assert_false (code_sparse_set_contains (&set, capacity));
  for (guint i = 1; i < capacity; i++)
    g_assert_true (code_sparse_set_contains (&set, i));

  code_sparse_set_sort (&set);

  g_assert_false (code_sparse_set_contains (&set, 0));
  g_assert_false (code_sparse_set_contains (&set, capacity));
  for (guint i = 1; i < capacity; i++)
    g_assert_true (code_sparse_set_contains (&set, i));

  for (guint i = 1; i < capacity; i++)
    {
      g_assert_cmpint (set.dense[i-1].value, ==, i);
      g_assert_cmpint (set.dense[i-1].user_value, ==, i+1024);
    }

  code_sparse_set_clear (&set);
}

int
main (int argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/CodeSparseSet/basic", test_sparse_set);
  return g_test_run ();
}
