/* code-version-macros.h
 *
 * Copyright 2022 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib.h>

#include "code-version.h"

#ifndef _CODE_EXTERN
# define _CODE_EXTERN extern
#endif

#define CODE_VERSION_CUR_STABLE (G_ENCODE_VERSION (CODE_MAJOR_VERSION, 0))

#ifdef CODE_DISABLE_DEPRECATION_WARNINGS
# define CODE_DEPRECATED _CODE_EXTERN
# define CODE_DEPRECATED_FOR(f) _CODE_EXTERN
# define CODE_UNAVAILABLE(maj,min) _CODE_EXTERN
#else
# define CODE_DEPRECATED G_DEPRECATED _CODE_EXTERN
# define CODE_DEPRECATED_FOR(f) G_DEPRECATED_FOR (f) _CODE_EXTERN
# define CODE_UNAVAILABLE(maj,min) G_UNAVAILABLE (maj, min) _CODE_EXTERN
#endif

#define CODE_VERSION_1_0 (G_ENCODE_VERSION (1, 0))

#if CODE_MAJOR_VERSION == CODE_VERSION_1_0
# define CODE_VERSION_PREV_STABLE (CODE_VERSION_1_0)
#else
# define CODE_VERSION_PREV_STABLE (G_ENCODE_VERSION (CODE_MAJOR_VERSION - 1, 0))
#endif

/**
 * CODE_VERSION_MIN_REQUIRED:
 *
 * A macro that should be defined by the user prior to including
 * the code.h header.
 *
 * The definition should be one of the predefined CODE version
 * macros: %CODE_VERSION_1_0, ...
 *
 * This macro defines the lower bound for the Builder API to use.
 *
 * If a function has been deprecated in a newer version of Builder,
 * it is possible to use this symbol to avoid the compiler warnings
 * without disabling warning for every deprecated function.
 */
#ifndef CODE_VERSION_MIN_REQUIRED
# define CODE_VERSION_MIN_REQUIRED (CODE_VERSION_CUR_STABLE)
#endif

/**
 * CODE_VERSION_MAX_ALLOWED:
 *
 * A macro that should be defined by the user prior to including
 * the code.h header.

 * The definition should be one of the predefined Builder version
 * macros: %CODE_VERSION_1_0, %CODE_VERSION_1_2,...
 *
 * This macro defines the upper bound for the CODE API to use.
 *
 * If a function has been introduced in a newer version of Builder,
 * it is possible to use this symbol to get compiler warnings when
 * trying to use that function.
 */
#ifndef CODE_VERSION_MAX_ALLOWED
# if CODE_VERSION_MIN_REQUIRED > CODE_VERSION_PREV_STABLE
#  define CODE_VERSION_MAX_ALLOWED (CODE_VERSION_MIN_REQUIRED)
# else
#  define CODE_VERSION_MAX_ALLOWED (CODE_VERSION_CUR_STABLE)
# endif
#endif

#define CODE_AVAILABLE_IN_ALL _CODE_EXTERN

#if CODE_VERSION_MIN_REQUIRED >= CODE_VERSION_1_0
# define CODE_DEPRECATED_IN_1_0 CODE_DEPRECATED
# define CODE_DEPRECATED_IN_1_0_FOR(f) CODE_DEPRECATED_FOR(f)
#else
# define CODE_DEPRECATED_IN_1_0 _CODE_EXTERN
# define CODE_DEPRECATED_IN_1_0_FOR(f) _CODE_EXTERN
#endif
#if CODE_VERSION_MAX_ALLOWED < CODE_VERSION_1_0
# define CODE_AVAILABLE_IN_1_0 CODE_UNAVAILABLE(1, 0)
#else
# define CODE_AVAILABLE_IN_1_0 _CODE_EXTERN
#endif
