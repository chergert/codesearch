/*
 * codesearch.c
 *
 * Copyright 2022-2023 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include <locale.h>
#include <unistd.h>

#include <glib/gi18n.h>
#include <glib/gstdio.h>

#include <libcodesearch.h>
#include <libdex.h>
#include <libgit2-glib/ggit.h>

#include "code-line-reader.h"

#define MAX_TRIGRAMS (1<<14)

static GRegex     *ignore_regex;
static const char *ignore_regexes[] = {
  "\\.a$",
  "\\.bak$",
  "\\.bz2$",
  "\\.dat$",
  "\\.dll$",
  "\\.dylib$",
  "\\.exe$",
  "\\.flatpak-builder$",
  "\\.gir$",
  "\\.git$",
  "\\.gmo$",
  "\\.gz$",
  "\\.iso$",
  "\\.jpeg$",
  "\\.jpg$",
  "\\.la$",
  "\\.o$",
  "\\.png$",
  "\\.po$",
  "\\.pyc$",
  "\\.pyo$",
  "\\.so$",
  "\\.so([\\.0-9])+$",
  "\\.svg$",
  "\\.svn$",
  "\\.tar\\.*$",
  "\\.typelib$",
  "~$",
};

static char **index_recurse;
static char **index_directories;
static char **index_git;
static char *index_output;
static char *index_relative_to;
static gboolean index_diffs;
static GFile *index_relative_to_file;
static DexChannel *index_channel;
static DexScheduler *index_thread_pool;
static GMutex index_builders_mutex;
static GPrivate index_builders_key = G_PRIVATE_INIT (NULL);
static GPtrArray *index_builders;
static guint index_active;
static const GOptionEntry index_entries[] = {
  { "directory", 'd', 0, G_OPTION_ARG_FILENAME_ARRAY, &index_directories,
    N_("Non-recursively index DIR. May repeat."), N_("DIR") },
  { "recurse", 'r', 0, G_OPTION_ARG_FILENAME_ARRAY, &index_recurse,
    N_("Recursively index starting from DIR. May repeat."), N_("DIR") },
  { "git", 'g', 0, G_OPTION_ARG_FILENAME_ARRAY, &index_git,
    N_("Index commits from git repository"), N_("GITDIR") },
  { "diff", 'i', 0, G_OPTION_ARG_NONE, &index_diffs,
    N_("Index commit diffs from git repository") },
  { "output", 'o', 0, G_OPTION_ARG_FILENAME, &index_output,
    N_("Write index to FILE pattern."), N_("FILE") },
  { "relative-to", 't', 0, G_OPTION_ARG_FILENAME, &index_relative_to,
    N_("Attempt to encode paths relative to DIR"), N_("DIR") },
  { NULL }
};

typedef struct _IndexJob
{
  GFile          *file;
  GgitCommit     *commit;
  GgitCommit     *parent_commit;
  GgitRepository *repository;
  guint           is_directory : 1;
  guint           is_recursive : 1;
  guint           is_git : 1;
} IndexJob;

static IndexJob *
index_job_new (GFile      *file,
               gboolean    is_directory,
               gboolean    is_recursive)
{
  IndexJob *job = g_new0 (IndexJob, 1);

  g_atomic_int_inc (&index_active);

  job->file = file;
  job->commit = NULL;
  job->parent_commit = NULL;
  job->repository = NULL;
  job->is_directory = !!is_directory;
  job->is_recursive = !!is_recursive;
  job->is_git = FALSE;

  return job;
}

static IndexJob *
index_job_new_commit (GgitRepository *repository,
                      GgitCommit     *commit,
                      GgitCommit     *parent_commit)
{
  IndexJob *job = g_new0 (IndexJob, 1);

  g_atomic_int_inc (&index_active);

  job->repository = g_object_ref (repository);
  job->commit = g_object_ref (commit);
  job->parent_commit = g_object_ref (commit);
  job->is_directory = FALSE;
  job->is_recursive = FALSE;
  job->is_git = TRUE;

  return job;
}

static void
index_job_free (IndexJob *job)
{
  g_clear_object (&job->file);
  g_clear_object (&job->commit);
  g_clear_object (&job->parent_commit);
  g_clear_object (&job->repository);
  g_free (job);

  g_atomic_int_add (&index_active, -1);
}

static int
append_diff_cb (GgitDiffDelta *delta,
                GgitDiffHunk  *hunk,
                GgitDiffLine  *line,
                gpointer       user_data)
{
  GString *string = user_data;
  const guint8 *content;
  GgitDiffLineType type;
  gsize len;

  content = ggit_diff_line_get_content (line, &len);
  type = ggit_diff_line_get_origin (line);

  if (type == GGIT_DIFF_LINE_ADDITION)
    g_string_append_c (string, '+');
  else if (type == GGIT_DIFF_LINE_DELETION)
    g_string_append_c (string, '-');
  else if (type == GGIT_DIFF_LINE_CONTEXT)
    g_string_append_c (string, ' ');

  g_string_append_len (string, (const char *)content, len);

  return 0;
}

static void
append_diff (GString        *string,
             GgitRepository *repository,
             GgitCommit     *commit,
             GgitCommit     *parent_commit)
{
  static GgitDiffOptions *options;
  g_autoptr(GgitTree) tree_a = NULL;
  g_autoptr(GgitTree) tree_b = NULL;
  g_autoptr(GgitDiff) diff = NULL;

  if (g_once_init_enter (&options))
    {
      GgitDiffOptions *opts = ggit_diff_options_new ();
      ggit_diff_options_set_flags (opts,
                                   (GGIT_DIFF_IGNORE_WHITESPACE |
                                    GGIT_DIFF_IGNORE_WHITESPACE_CHANGE |
                                    GGIT_DIFF_IGNORE_FILE_MODE |
                                    GGIT_DIFF_IGNORE_SUBMODULES |
                                    GGIT_DIFF_SKIP_BINARY_CHECK |
                                    GGIT_DIFF_ENABLE_FAST_UNTRACKED_DIRS |
                                    GGIT_DIFF_IGNORE_WHITESPACE_EOL));
      ggit_diff_options_set_n_context_lines (opts, 0);
      ggit_diff_options_set_n_interhunk_lines (opts, 0);
      g_once_init_leave (&options, opts);
    }

  tree_a = ggit_commit_get_tree (parent_commit);
  tree_b = ggit_commit_get_tree (commit);

  if (!(diff = ggit_diff_new_tree_to_tree (repository, tree_a, tree_b, options, NULL)))
    return;

  ggit_diff_print (diff, GGIT_DIFF_FORMAT_PATCH, append_diff_cb, (gpointer)string, NULL);
}

static char *
build_commit_message (const char     *oidstr,
                      GgitRepository *repository,
                      GgitCommit     *commit,
                      GgitCommit     *parent_commit,
                      gsize          *len)
{
  g_autoptr(GString) string = g_string_new (NULL);
  g_autoptr(GgitSignature) author = NULL;

  g_string_append_printf (string, "commit %s\n", oidstr);

  if ((author = ggit_commit_get_author (commit)))
    {
      g_autoptr(GDateTime) dt = ggit_signature_get_time (author);
      g_autofree char *date = g_date_time_format (dt, "%a %b %e %H:%M:%S %Y %z");

      g_string_append_printf (string,
                              "Author: %s <%s>\n"
                              "Date:   %s\n"
                              "\n",
                              ggit_signature_get_name (author),
                              ggit_signature_get_email (author),
                              date);
    }

  g_string_append (string, ggit_commit_get_message (commit));
  g_string_append_c (string, '\n');

  if (index_diffs && commit && parent_commit)
    append_diff (string, repository, commit, parent_commit);

  *len = string->len;

  return g_string_free (g_steal_pointer (&string), FALSE);
}

static DexFuture *
git_document_loader (CodeIndex  *index,
                     const char *path,
                     gpointer    user_data)
{
  GgitRepository *repository = user_data;
  g_autoptr(GgitOId) oid = ggit_oid_new_from_string (path);
  g_autoptr(GgitCommit) commit = NULL;
  g_autoptr(GError) error = NULL;
  char *str;
  gsize len;

  if (oid == NULL)
    return dex_future_new_reject (G_IO_ERROR,
                                  G_IO_ERROR_FAILED,
                                  "Invalid Git OId: %s",
                                  path);

  if (!(commit = ggit_repository_lookup_commit (repository, oid, &error)))
    return dex_future_new_for_error (g_steal_pointer (&error));

  /* TODO: Get parent commit for searching if necessary */
  str = build_commit_message (path, repository, commit, NULL, &len);

  return dex_future_new_take_boxed (G_TYPE_BYTES, g_bytes_new_take (str, len));
}

static DexFuture *
relative_document_loader (CodeIndex  *index,
                          const char *path,
                          gpointer    user_data)
{
  const char *basedir = user_data;
  g_autofree char *full_path = g_build_filename (basedir, path, NULL);
  g_autoptr(GMappedFile) mapped = g_mapped_file_new (full_path, FALSE, NULL);

  if (mapped != NULL)
    return dex_future_new_take_boxed (G_TYPE_BYTES,
                                      g_mapped_file_get_bytes (mapped));

  return dex_future_new_reject (G_IO_ERROR,
                                G_IO_ERROR_FAILED,
                                "Failed to load memory map for %s",
                                full_path);
}

static DexFuture *
index_job_submission_fiber (gpointer data)
{
  GPtrArray *jobs = data;

  while (jobs->len > 0)
    {
      IndexJob *job = g_ptr_array_steal_index (jobs, jobs->len-1);
      dex_await (dex_channel_send (index_channel, dex_future_new_for_pointer (job)), NULL);
    }

  return NULL;
}

static void
index_job_run_directory (IndexJob   *job,
                         DexChannel *channel)
{
  g_autoptr(GFileEnumerator) enumerator =
    dex_await_object (dex_file_enumerate_children (job->file,
                                                   G_FILE_ATTRIBUTE_STANDARD_TYPE,
                                                   G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                                                   0),
                      NULL);
  GList *infos;

  g_printerr ("Indexing directory %s\n", g_file_peek_path (job->file));

  while ((infos = dex_await_boxed (dex_file_enumerator_next_files (enumerator, 100, 0), NULL)))
    {
      for (const GList *iter = infos; iter; iter = iter->next)
        {
          GFileInfo *info = iter->data;
          GFileType file_type = g_file_info_get_file_type (info);

          if (file_type == G_FILE_TYPE_REGULAR)
            dex_await (dex_channel_send (index_channel,
                                         dex_future_new_for_pointer (index_job_new (g_file_enumerator_get_child (enumerator, info),
                                                                                    FALSE, FALSE))),
                       NULL);
          else if (job->is_recursive && file_type == G_FILE_TYPE_DIRECTORY)
            dex_await (dex_channel_send (index_channel,
                                         dex_future_new_for_pointer (index_job_new (g_file_enumerator_get_child (enumerator, info),
                                                                                    TRUE, TRUE))),
                       NULL);

        }

      g_list_free_full (infos, g_object_unref);
    }
}

static CodeIndexBuilder *
get_builder_for_fiber (void)
{
  CodeIndexBuilder *builder = g_private_get (&index_builders_key);

  if G_UNLIKELY (builder == NULL)
    {
      builder = code_index_builder_new ();

      g_mutex_lock (&index_builders_mutex);
      g_ptr_array_add (index_builders, builder);
      g_mutex_unlock (&index_builders_mutex);

      g_private_set (&index_builders_key, builder);
    }

  return builder;
}

static void
index_job_run_file (IndexJob *job)
{
  g_autoptr(GBytes) bytes = NULL;
  g_autofree char *relpath = NULL;
  CodeIndexBuilder *builder;
  CodeTrigramIter iter;
  CodeTrigram trigram;
  const char *path;
  const char *data;
  gsize len;

  if (!(bytes = dex_await_boxed (dex_file_load_contents_bytes (job->file), NULL)))
    return;

  data = g_bytes_get_data (bytes, &len);

  builder = get_builder_for_fiber ();

  if (index_relative_to_file != NULL &&
      g_file_has_prefix (job->file, index_relative_to_file))
    path = relpath = g_file_get_relative_path (index_relative_to_file, job->file);
  else
    path = g_file_peek_path (job->file);

  code_index_builder_begin (builder, path);

  code_trigram_iter_init (&iter, data, len);

  while (code_trigram_iter_next (&iter, &trigram))
    {
      code_index_builder_add (builder, &trigram);

      if (code_index_builder_get_uncommitted (builder) > MAX_TRIGRAMS)
        {
          code_index_builder_rollback (builder);
          return;
        }
    }

  code_index_builder_commit (builder);
}

static void
index_job_run_git (IndexJob   *job,
                   DexChannel *channel)
{
  g_autoptr(GgitRepository) repo = NULL;
  g_autoptr(GgitRevisionWalker) walker = NULL;
  g_autoptr(GFile) discover = NULL;
  g_autoptr(GError) error = NULL;
  GgitOId *oid;

  if (!(discover = ggit_repository_discover_full (job->file, TRUE, NULL, &error)))
    return;

  if (!(repo = ggit_repository_open (discover, &error)))
    return;

  if (!(walker = ggit_revision_walker_new (repo, &error)))
    return;

  ggit_revision_walker_push_head (walker, NULL);

  while ((oid = ggit_revision_walker_next (walker, NULL)))
    {
      g_autoptr(GgitCommitParents) parents = NULL;
      g_autoptr(GgitCommit) commit = NULL;
      g_autoptr(GgitCommit) parent_commit = NULL;

      if (!(commit =  ggit_repository_lookup_commit (repo, oid, NULL)) ||
          !(parents = ggit_commit_parents_new (commit)) ||
          ggit_commit_parents_get_size (parents) > 1 ||
          !(parent_commit = ggit_commit_parents_get (parents, 0)))
        continue;

      dex_await (dex_channel_send (channel,
                                   dex_future_new_for_pointer (index_job_new_commit (repo, commit, parent_commit))),
                 NULL);
    }
}

static void
index_job_run_commit (IndexJob *job)
{
  gsize len;
  g_autoptr(GgitOId) oid = ggit_object_get_id (GGIT_OBJECT (job->commit));
  g_autofree char *oidstr = ggit_oid_to_string (oid);
  g_autofree char *str = build_commit_message (oidstr, job->repository, job->commit, job->parent_commit, &len);
  CodeIndexBuilder *builder = get_builder_for_fiber ();
  CodeTrigramIter iter;
  CodeTrigram trigram;

  code_index_builder_begin (builder, oidstr);
  code_trigram_iter_init (&iter, str, len);
  while (code_trigram_iter_next (&iter, &trigram))
    code_index_builder_add (builder, &trigram);
  code_index_builder_commit (builder);
}

static DexFuture *
index_job_worker_fiber (gpointer data)
{
  DexChannel *channel = data;
  IndexJob *job;

  /* Wait for another job to come in, or send-side-close */
  while ((job = dex_await_pointer (dex_channel_receive (channel), NULL)))
    {
      if (job->commit)
        index_job_run_commit (job);
      else if (job->is_git)
        index_job_run_git (job, channel);
      else if (!g_regex_match (ignore_regex, g_file_peek_path (job->file), 0, NULL))
        {
          if (job->is_directory)
            index_job_run_directory (job, channel);
          else
            index_job_run_file (job);
        }

      index_job_free (job);

      if (g_atomic_int_get (&index_active) == 0)
        dex_channel_close_send (channel);
    }

  return NULL;
}

static DexFuture *
index_job_merge (DexFuture *completed,
                 gpointer   user_data)
{
  g_autoptr(CodeIndexBuilder) builder = code_index_builder_new ();
  GPtrArray *ar = user_data;

  for (guint i = 0; i < ar->len; i++)
    {
      g_autofree char *filename = g_strdup_printf ("%s.%u", index_output, i);
      g_autoptr(GError) error = NULL;
      g_autoptr(CodeIndex) index = code_index_new (filename, &error);

      if (index == NULL)
        g_printerr ("Failed to reload index: %s: %s\n", filename, error->message);
      else if (!code_index_builder_merge (builder, index))
        g_printerr ("Failed to merge index: %s\n", filename);
      else
        g_unlink (filename);
    }

  return code_index_builder_write_filename (builder, index_output, 0);
}

static DexFuture *
index_job_writer (DexFuture *completed,
                  gpointer   user_data)
{
  g_autoptr(GPtrArray) futures = g_ptr_array_new_with_free_func (dex_unref);
  GPtrArray *ar = user_data;

  for (guint i = 0; i < ar->len; i++)
    {
      CodeIndexBuilder *builder = g_ptr_array_index (ar, i);
      g_autofree char *filename = g_strdup_printf ("%s.%u", index_output, i);
      g_autoptr(GFile) file = g_file_new_for_path (filename);
      g_autoptr(GFileOutputStream) stream = g_file_replace (file, NULL, FALSE, 0, NULL, NULL);

      g_ptr_array_add (futures, code_index_builder_write (builder, G_OUTPUT_STREAM (stream), 0));
    }

  if (futures->len == 0)
    return dex_future_new_for_boolean (TRUE);

  return dex_future_then (dex_future_allv ((DexFuture **)futures->pdata, futures->len),
                          index_job_merge,
                          g_ptr_array_ref (ar),
                          (GDestroyNotify)g_ptr_array_unref);
}

static DexFuture *
quit_cb (DexFuture *future,
         gpointer   user_data)
{
  GMainLoop **main_loop = user_data;
  g_main_loop_quit (*main_loop);
  g_clear_pointer (main_loop, g_main_loop_unref);
  return NULL;
}

static int
codesearch_index (int    argc,
                  char **argv)
{
  g_autoptr(GOptionContext) context = g_option_context_new ("- Index files or directories");
  g_autoptr(GMainLoop) main_loop = g_main_loop_new (NULL, FALSE);
  g_autoptr(GPtrArray) jobs = g_ptr_array_new_with_free_func ((GDestroyNotify)index_job_free);
  g_autoptr(GPtrArray) workers = g_ptr_array_new_with_free_func (dex_unref);
  g_autoptr(DexFuture) submissions = NULL;
  g_autoptr(DexFuture) future = NULL;
  g_autoptr(GString) regex_str = NULL;
  g_autoptr(GError) error = NULL;
  const int n_workers = 256;

  g_option_context_add_main_entries (context, index_entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      g_printerr ("usage: %s [OPTIONS...]\n", argv[0]);
      return EXIT_FAILURE;
    }

  if (index_output == NULL)
    index_output = g_strdup ("index");

  g_mutex_init (&index_builders_mutex);
  index_builders = g_ptr_array_new_with_free_func ((GDestroyNotify)code_index_builder_unref);

  index_channel = dex_channel_new (G_MAXINT);
  index_thread_pool = dex_thread_pool_scheduler_new ();

  if (index_relative_to)
    index_relative_to_file = g_file_new_for_commandline_arg (index_relative_to);

  regex_str = g_string_new ("(");
  for (guint i = 0; i < G_N_ELEMENTS (ignore_regexes); i++)
    {
      if (i != 0)
        g_string_append_c (regex_str, '|');
      g_string_append_printf (regex_str, "(%s)", ignore_regexes[i]);
    }
  g_string_append_c (regex_str, ')');
  ignore_regex = g_regex_new (regex_str->str, G_REGEX_CASELESS|G_REGEX_OPTIMIZE, 0, &error);
  g_assert_no_error (error);

  if (index_recurse != NULL)
    {
      for (guint i = 0; index_recurse[i]; i++)
        g_ptr_array_add (jobs, index_job_new (g_file_new_for_commandline_arg (index_recurse[i]), TRUE, TRUE));
    }

  if (index_directories != NULL)
    {
      for (guint i = 0; index_directories[i]; i++)
        g_ptr_array_add (jobs, index_job_new (g_file_new_for_commandline_arg (index_directories[i]), TRUE, FALSE));
    }

  if (index_git != NULL)
    {
      for (guint i = 0; index_git[i]; i++)
        {
          IndexJob *job = index_job_new (g_file_new_for_commandline_arg (index_git[i]), FALSE, FALSE);
          job->is_git = TRUE;
          g_ptr_array_add (jobs, job);
        }
    }

  for (int i = 1; i < argc; i++)
    g_ptr_array_add (jobs, index_job_new (g_file_new_for_commandline_arg (argv[i]), FALSE, FALSE));

  if (jobs->len == 0)
    {
      g_printerr ("No files provided to index\n");
      return EXIT_FAILURE;
    }

  submissions = dex_scheduler_spawn (NULL, 0,
                                     index_job_submission_fiber,
                                     g_ptr_array_ref (jobs),
                                     (GDestroyNotify)g_ptr_array_unref);
  for (guint i = 0; i < n_workers; i++)
    g_ptr_array_add (workers,
                     dex_scheduler_spawn (index_thread_pool, 0,
                                          index_job_worker_fiber,
                                          dex_ref (index_channel),
                                          dex_unref));

  future = dex_future_all (dex_ref (submissions),
                           dex_future_allv ((DexFuture **)workers->pdata, workers->len),
                           NULL);
  future = dex_future_finally (future,
                               index_job_writer,
                               g_ptr_array_ref (index_builders),
                               (GDestroyNotify)g_ptr_array_unref);
  future = dex_future_finally (future, quit_cb, &main_loop, NULL);

  if (main_loop != NULL)
    g_main_loop_run (main_loop);

  return EXIT_SUCCESS;
}

static char *query_relative_to;
static char *query_git;
static GgitRepository *query_git_repo;
static const GOptionEntry query_entries[] = {
  /* Necessary if you've indexed with relative-to */
  { "relative-to", 't', 0, G_OPTION_ARG_FILENAME, &query_relative_to, "Treat index as relative to DIR", "DIR" },
  { "git", 'g', 0, G_OPTION_ARG_FILENAME, &query_git, "Use git repo at DIR to locate commits in index", "DIR" },
  { NULL }
};

static const char *
next_match (CodeIndexIter *iters,
            guint          n_iters)
{
  CodeDocument document;

again:
  if (!code_index_iter_next (&iters[0], &document))
    return FALSE;

  for (guint i = 1; i < n_iters; i++)
    {
      if (!code_index_iter_seek_to (&iters[i], document.id))
        goto again;
    }

  return document.path;
}

typedef struct _QueryJob
{
  char   *path;
  GRegex *regex;
  GBytes *bytes;
} QueryJob;

static QueryJob *
query_job_new (const char *path,
               GRegex     *regex,
               GBytes     *bytes)
{
  QueryJob *job = g_new0 (QueryJob, 1);
  job->path = g_strdup (path);
  job->regex = g_regex_ref (regex);
  job->bytes = bytes;
  return job;
}

static void
query_job_free (QueryJob *job)
{
  g_clear_pointer (&job->path, g_free);
  g_clear_pointer (&job->regex, g_regex_unref);
  g_clear_pointer (&job->bytes, g_bytes_unref);
  g_free (job);
}

static void
run_query (const char *path,
           GRegex     *query,
           const char *data,
           gsize       data_len)
{
  g_autoptr(GString) str = g_string_new (path);
  g_autoptr(GMatchInfo) match_info = NULL;
  gsize trunc;

  g_string_append_c (str, ':');
  trunc = str->len;

  if (g_regex_match_full (query, data, data_len, 0, 0, &match_info, NULL))
    {
      const char *endptr = data+data_len;
      int begin;
      int end;

      while (g_match_info_fetch_pos (match_info, 0, &begin, &end))
        {
          const char *line_begin = data+begin;
          const char *line_end = data+end;

          while (line_begin > data && line_begin[0] != '\n')
            line_begin--;

          if (line_begin[0] == '\n')
            line_begin++;

          while (line_end[0] != '\n' && line_end+1 < endptr)
            line_end++;

          g_string_append_len (str, line_begin, line_end-line_begin);
          g_string_append_c (str, '\n');
          write (STDOUT_FILENO, str->str, str->len);
          str->len = trunc;

          if (!g_match_info_next (match_info, NULL))
            break;
        }
    }
}

static DexFuture *
query_job_fiber (gpointer user_data)
{
  QueryJob *job = user_data;
  const guint8 *data;
  gsize len;

  data = g_bytes_get_data (job->bytes, &len);
  run_query (job->path, job->regex, (const char *)data, len);

  return dex_future_new_for_boolean (TRUE);
}

static DexFuture *
query_index (CodeIndex *index,
             GArray    *trigrams,
             GRegex    *regex)
{
  g_autoptr(GPtrArray) ar = g_ptr_array_new_with_free_func (dex_unref);
  CodeIndexIter iters[trigrams->len];
  const char *path;

  for (guint i = 0; i < trigrams->len; i++)
    {
      const CodeTrigram *trigram = &g_array_index (trigrams, CodeTrigram, i);

      if (!code_index_iter_init (&iters[i], index, trigram))
        return dex_future_new_reject (G_IO_ERROR, G_IO_ERROR_FAILED, "No matches");
    }

  while ((path = next_match (iters, trigrams->len)))
    {
      g_autoptr(GBytes) bytes = dex_await_boxed (code_index_load_document_path (index, path), NULL);

      if (bytes != NULL)
        g_ptr_array_add (ar,
                         dex_scheduler_spawn (NULL, 0,
                                              query_job_fiber,
                                              query_job_new (path, regex, g_steal_pointer (&bytes)),
                                              (GDestroyNotify)query_job_free));
    }

  if (ar->len == 0)
    return dex_future_new_for_boolean (TRUE);

  return dex_future_allv ((DexFuture **)ar->pdata, ar->len);
}

static inline gboolean
skip_trigram (const CodeTrigram *trigram)
{
  static gunichar ignored[] = { '^', '$' };

  for (guint i = 0; i < G_N_ELEMENTS(ignored); i++)
    return trigram->x == ignored[i] ||
           trigram->y == ignored[i] ||
           trigram->z == ignored[i];
  return FALSE;
}

static int
codesearch_query (int   argc,
                  char *argv[])
{
  g_autoptr(GOptionContext) context = g_option_context_new ("QUERY INDEX [INDEX...] - Query search indexes");
  g_autoptr(GPtrArray) futures = g_ptr_array_new_with_free_func (dex_unref);
  g_autoptr(GArray) trigrams = g_array_new (FALSE, FALSE, sizeof (CodeTrigram));
  g_autoptr(GError) error = NULL;
  g_autoptr(GRegex) regex = NULL;
  CodeTrigramIter iter;
  CodeTrigram trigram;
  const char *query_str;
  int ret = EXIT_SUCCESS;

  g_option_context_add_main_entries (context, query_entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      goto usage;
    }

  if (argc < 3)
    goto usage;

  if (query_git)
    {
      g_autoptr(GFile) location = g_file_new_for_commandline_arg (query_git);
      g_autoptr(GFile) repodir = ggit_repository_discover (location, &error);

      index_diffs = TRUE;

      if (repodir == NULL)
        {
          g_printerr ("Failed to locate git repository at %s: %s\n",
                      query_git, error->message);
          return EXIT_FAILURE;
        }

      if (!(query_git_repo = ggit_repository_open (repodir, &error)))
        {
          g_printerr ("Failed to open git repository at %s: %s\n",
                      query_git, error->message);
          return EXIT_FAILURE;
        }
    }

  /* TODO: Create CodeQuery structure for query language. We would
   * really want to parse out the regex up front so that we can just
   * include the grams we care about.
   */
  query_str = argv[1];
  code_trigram_iter_init (&iter, query_str, -1);
  while (code_trigram_iter_next (&iter, &trigram))
    {
      if (!skip_trigram (&trigram))
        g_array_append_val (trigrams, trigram);
    }

  if (!(regex = g_regex_new (query_str, G_REGEX_MULTILINE|G_REGEX_OPTIMIZE, 0, &error)))
    {
      g_printerr ("Invalid regex: %s\n", error->message);
      return EXIT_FAILURE;
    }

  for (int i = 2; i < argc; i++)
    {
      g_autoptr(CodeIndex) cindex = code_index_new (argv[i], &error);

      if (error != NULL)
        {
          g_printerr ("%s: %s\n", argv[i], error->message);
          ret = EXIT_FAILURE;
          g_clear_error (&error);
          continue;
        }

      if (query_git_repo)
        code_index_set_document_loader (cindex,
                                        git_document_loader,
                                        g_object_ref (query_git_repo),
                                        g_object_unref);
      else if (query_relative_to)
        code_index_set_document_loader (cindex,
                                        relative_document_loader,
                                        g_strdup (query_relative_to),
                                        g_free);

      g_ptr_array_add (futures, query_index (cindex, trigrams, regex));
    }

  if (futures->len > 0)
    {
      g_autoptr(DexFuture) future = NULL;
      g_autoptr(GMainLoop) main_loop = g_main_loop_new (NULL, FALSE);

      future = dex_future_allv ((DexFuture **)futures->pdata, futures->len);
      future = dex_future_finally (future, quit_cb, &main_loop, NULL);

      if (main_loop != NULL)
        g_main_loop_run (main_loop);
    }

  return ret;

usage:
  g_printerr ("usage: %s [OPTIONS...] QUERY INDEX [INDEX...]\n", argv[0]);
  return EXIT_FAILURE;
}

static char *merge_output = NULL;
static const GOptionEntry merge_entries[] = {
  { "output", 'o', 0, G_OPTION_ARG_FILENAME, &merge_output, "Write index into FILE", "FILE" },
  { NULL }
};

static int
codesearch_merge (int   argc,
                  char *argv[])
{
  g_autoptr(GOptionContext) context = g_option_context_new ("-o FILE INDEX... - Merge indexes into");
  g_autoptr(CodeIndexBuilder) builder = NULL;
  g_autoptr(GMainLoop) main_loop = NULL;
  g_autoptr(DexFuture) future = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) file = NULL;
  int ret = EXIT_SUCCESS;

  g_option_context_add_main_entries (context, merge_entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      goto usage;
    }

  if (argc < 2 || merge_output == NULL || merge_output[0] == 0)
    goto usage;

  main_loop = g_main_loop_new (NULL, FALSE);
  builder = code_index_builder_new ();
  file = g_file_new_for_commandline_arg (merge_output);

  for (int i = 1; i < argc; i++)
    {
      g_autoptr(CodeIndex) index = code_index_new (argv[i], &error);

      if (index == NULL)
        {
          g_printerr ("Failed to load index: %s\n", error->message);
          return EXIT_FAILURE;
        }

      if (!code_index_builder_merge (builder, index))
        {
          g_printerr ("Cannot merge index, not enough space for documents\n");
          return EXIT_FAILURE;
        }
    }

  future = code_index_builder_write_file (builder, file, 0);
  future = dex_future_finally (future, quit_cb, &main_loop, NULL);

  if (main_loop != NULL)
    g_main_loop_run (main_loop);

  return ret;

usage:
  g_printerr ("usage: %s -o FILE INDEXES...\n", argv[0]);
  return EXIT_FAILURE;
}

static gboolean info_list_documents;
static const GOptionEntry info_entries[] = {
  { "list-documents", 'l', 0, G_OPTION_ARG_NONE, &info_list_documents, "List documents within index" },
  { NULL }
};

typedef struct _InfoDocument
{
  const char *path;
  char *key;
} InfoDocument;

static void
clear_info_document (gpointer data)
{
  InfoDocument *doc = data;
  g_clear_pointer (&doc->key, g_free);
}

static int
compare_by_path (gconstpointer a,
                 gconstpointer b)
{
  const InfoDocument *adoc = (const InfoDocument *)a;
  const InfoDocument *bdoc = (const InfoDocument *)b;

  return strcmp (adoc->key, bdoc->key);
}

static int
codesearch_info (int   argc,
                 char *argv[])
{
  g_autoptr(GOptionContext) context = g_option_context_new ("INDEX... - Print info about indexes");
  g_autoptr(GError) error = NULL;

  g_option_context_add_main_entries (context, info_entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      goto usage;
    }

  if (argc < 2)
    goto usage;

  for (int i = 1; i < argc; i++)
    {
      g_autoptr(CodeIndex) index = code_index_new (argv[i], &error);
      g_autofree char *n_documents_bytes_str = NULL;
      g_autofree char *n_trigrams_bytes_str = NULL;
      g_autofree char *trigrams_data_bytes_str = NULL;
      CodeIndexStat stat;

      if (index == NULL)
        {
          g_printerr ("%s: %s\n", argv[i], error->message);
          continue;
        }

      code_index_stat (index, &stat);

      n_documents_bytes_str = g_format_size (stat.n_documents_bytes);
      n_trigrams_bytes_str = g_format_size (stat.n_trigrams_bytes);
      trigrams_data_bytes_str = g_format_size (stat.trigrams_data_bytes);

      if (i > 1)
        g_print ("\n");

      g_print ("              Index Filename:  %s\n", argv[i]);
      g_print ("             Documents Count:  %u\n", stat.n_documents);
      g_print ("              Trigrams Count:  %u\n", stat.n_trigrams);
      g_print ("      Documents Section Size:  %s\n", n_documents_bytes_str);
      g_print ("       Trigrams Section Size:  %s\n", n_trigrams_bytes_str);
      g_print ("  Trigrams Data Section Size:  %s\n", trigrams_data_bytes_str);

      if (info_list_documents)
        {
          g_autoptr(GArray) paths = g_array_sized_new (FALSE, FALSE, sizeof (InfoDocument), stat.n_documents);
          guint pos = 0;

          for (guint d = 1; d < stat.n_documents; d++)
            {
              InfoDocument *doc = &g_array_index (paths, InfoDocument, pos);
              const char *path = code_index_get_document_path (index, d);

              if (path == NULL)
                {
                  paths->len--;
                  continue;
                }

              doc->path = path;
              doc->key = g_utf8_collate_key_for_filename (path, -1);

              pos++;
            }

          paths->len = pos;
          g_array_set_clear_func (paths, clear_info_document);
          g_array_sort (paths, compare_by_path);

          if (paths->len > 0)
            g_print ("\n");
          for (guint j = 0; j < paths->len; j++)
            {
              const InfoDocument *doc = &g_array_index (paths, InfoDocument, j);
              g_print ("%s\n", doc->path);
            }
        }
    }

  return EXIT_SUCCESS;

usage:
  g_printerr ("usage: %s -o FILE INDEXES...\n", argv[0]);
  return EXIT_FAILURE;
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr(GOptionContext) context = g_option_context_new ("COMMAND - A code search tool");
  g_autoptr(GError) error = NULL;

  ggit_init ();

  setlocale (LC_ALL, "");

  if (argc > 1)
    {
      const char *command = argv[1];

      if (g_str_equal (command, "index"))
        {
           argv[1] = g_strdup ("codesearch-index");
           return codesearch_index (argc-1, &argv[1]);
        }

      if (g_str_equal (command, "query"))
        {
           argv[1] = g_strdup ("codesearch-query");
           return codesearch_query (argc-1, &argv[1]);
        }

      if (g_str_equal (command, "merge"))
        {
           argv[1] = g_strdup ("codesearch-merge");
           return codesearch_merge (argc-1, &argv[1]);
        }

      if (g_str_equal (command, "info"))
        {
           argv[1] = g_strdup ("codesearch-info");
           return codesearch_info (argc-1, &argv[1]);
        }
    }

  g_option_context_set_summary (context,
"Commands:\n\
  index     index directories or files\n\
  query     search across indexes\n\
  merge     merge multiple indexes\n\
  info      print info about an index\n\
");

  if (!g_option_context_parse (context, &argc, &argv, &error))
    g_printerr ("%s\n", error->message);
  g_printerr ("usage: %s COMMAND [--help]\n", argv[0]);

  return EXIT_FAILURE;
}
