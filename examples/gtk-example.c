/* gtk-example.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <adwaita.h>
#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>

#include <libcodesearch.h>

#include "code-window.h"

int
main (int   argc,
      char *argv[])
{
  g_autoptr(GMainLoop) main_loop = NULL;
  CodeWindow *window;

  dex_init ();
  gtk_init ();
  adw_init ();
  gtk_source_init ();

  main_loop = g_main_loop_new (NULL, FALSE);
  window = code_window_new ();

  for (int i = 1; i < argc; i++)
    {
      g_autoptr(GError) error = NULL;
      CodeIndex *index = code_index_new (argv[i], &error);

      if (error != NULL)
        g_error ("Failed to open %s: %s", argv[i], error->message);

      code_window_add_index (window, index, "Files", "folder-symbolic");
    }

  g_signal_connect_swapped (window, "close-request", G_CALLBACK (g_main_loop_quit), main_loop);
  gtk_window_present (GTK_WINDOW (window));
  g_main_loop_run (main_loop);

  return 0;
}

