/*
 * code-window.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <gtksourceview/gtksource.h>

#include "code-window.h"

struct _CodeWindow
{
  AdwWindow           parent_instance;

  DexFuture          *loading;
  char               *loading_path;
  GCancellable       *cancellable;

  GtkSourceBuffer    *buffer;
  GtkSingleSelection *selection;
  GtkEditable        *entry;
  GtkRevealer        *revealer;
  GtkListBox         *indexes;
  AdwWindowTitle     *title;
  GtkSourceView      *view;
};

G_DEFINE_FINAL_TYPE (CodeWindow, code_window, ADW_TYPE_WINDOW)

enum {
  PROP_0,
  PROP_SHOW_PREVIEWS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static GPtrArray *
collect_indexes (CodeWindow *self)
{
  GPtrArray *ar;

  g_assert (CODE_IS_WINDOW (self));

  ar = g_ptr_array_new ();

  for (GtkWidget *child = gtk_widget_get_first_child (GTK_WIDGET (self->indexes));
       child != NULL;
       child = gtk_widget_get_next_sibling (child))
    {
      CodeIndex *index = g_object_get_data (G_OBJECT (child), "CODE_INDEX");

      if (!GTK_IS_LIST_BOX_ROW (child))
        continue;

      if (index && gtk_list_box_row_is_selected (GTK_LIST_BOX_ROW (child)))
        {
          if (ar->len)
            g_ptr_array_remove_range (ar, 0, ar->len);
          g_ptr_array_add (ar, index);
          return ar;
        }

      if (index)
        g_ptr_array_add (ar, index);
    }

  return ar;
}

static void
populate_cb (GObject      *object,
             GAsyncResult *result,
             gpointer      user_data)
{
  CodeResultSet *results = CODE_RESULT_SET (object);
  g_autoptr(CodeWindow) self = user_data;

  gtk_single_selection_set_model (self->selection, G_LIST_MODEL (results));
}

static void
on_search_changed_cb (CodeWindow  *self,
                      GtkEditable *editable)
{
  g_autoptr(CodeQuery) query = NULL;
  g_autoptr(CodeQuerySpec) spec = NULL;
  g_autoptr(CodeResultSet) results = NULL;
  g_autoptr(GPtrArray) indexes = NULL;
  const char *text;

  g_assert (CODE_IS_WINDOW (self));
  g_assert (GTK_IS_EDITABLE (editable));

  g_cancellable_cancel (self->cancellable);

  text = gtk_editable_get_text (editable);

  if (text[0] == 0)
    return;

  indexes = collect_indexes (self);

  spec = code_query_spec_new_contains (text);
  query = code_query_new (spec);
  results = code_result_set_new (query, (CodeIndex **)indexes->pdata, indexes->len);

  g_clear_object (&self->cancellable);
  self->cancellable = g_cancellable_new ();

  code_result_set_populate_async (results,
                                  dex_thread_pool_scheduler_get_default (),
                                  self->cancellable, populate_cb, g_object_ref (self));
}

static DexFuture *
on_document_loaded_cb (DexFuture *completed,
                       gpointer   user_data)
{
  CodeWindow *self = user_data;
  g_autoptr(GtkTextBuffer) buffer = NULL;

  g_assert (CODE_IS_WINDOW (self));
  g_assert (DEX_IS_FUTURE (completed));

  if ((buffer = dex_await_object (dex_ref (completed), NULL)))
    {
      g_autofree char *dir = g_path_get_dirname (self->loading_path);
      g_autofree char *name = g_path_get_basename (self->loading_path);

      gtk_text_view_set_buffer (GTK_TEXT_VIEW (self->view), buffer);
      adw_window_title_set_title (self->title, name);
      adw_window_title_set_subtitle (self->title, dir);

      g_object_set (self, "show-previews", TRUE, NULL);
    }

  return NULL;
}

static DexFuture *
load_document_fiber (gpointer user_data)
{
  CodeResult *result = user_data;
  CodeIndex *index = code_result_get_index (result);
  const char *path = code_result_get_path (result);
  g_autoptr(GBytes) bytes = NULL;
  g_autoptr(GtkSourceBuffer) buffer = NULL;

  bytes = dex_await_boxed (code_index_load_document_path (index, path), NULL);

  if (bytes == NULL)
    return NULL;

  buffer = gtk_source_buffer_new (NULL);
  gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buffer),
                            g_bytes_get_data (bytes, NULL),
                            g_bytes_get_size (bytes));

  return dex_future_new_take_object (g_steal_pointer (&buffer));
}

static void
on_selection_changed_cb (CodeWindow         *self,
                         GParamSpec         *pspec,
                         GtkSingleSelection *selection)
{
  g_autoptr(DexFuture) future = NULL;
  CodeResult *result = NULL;

  g_assert (CODE_IS_WINDOW (self));
  g_assert (GTK_IS_SINGLE_SELECTION (selection));

  if (!(result = gtk_single_selection_get_selected_item (selection)))
    {
      gtk_text_buffer_set_text (GTK_TEXT_BUFFER (self->buffer), "", 0);
      return;
    }

  dex_clear (&self->loading);

  g_set_str (&self->loading_path, code_result_get_path (result));

  future = dex_scheduler_spawn (dex_thread_pool_scheduler_get_default (), 0,
                                load_document_fiber, g_object_ref (result), g_object_unref);
  future = dex_future_then (future, on_document_loaded_cb, g_object_ref (self), g_object_unref);

  self->loading = g_steal_pointer (&future);
}

static void
indexes_header_func (GtkListBoxRow *row,
                     GtkListBoxRow *before,
                     gpointer       user_data)
{
  if (before != NULL && !g_object_get_data (G_OBJECT (before), "CODE_INDEX"))
    gtk_list_box_row_set_header (row,
                                 g_object_new (GTK_TYPE_SEPARATOR,
                                               "orientation", GTK_ORIENTATION_HORIZONTAL,
                                               NULL));
}

static void
code_window_finalize (GObject *object)
{
  CodeWindow *self = (CodeWindow *)object;

  dex_clear (&self->loading);
  g_clear_pointer (&self->loading_path, g_free);
  g_clear_object (&self->cancellable);

  G_OBJECT_CLASS (code_window_parent_class)->finalize (object);
}

static void
code_window_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  CodeWindow *self = CODE_WINDOW (object);

  switch (prop_id)
    {
    case PROP_SHOW_PREVIEWS:
      if (self->revealer)
        g_value_set_boolean (value, gtk_revealer_get_reveal_child (self->revealer));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
code_window_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  CodeWindow *self = CODE_WINDOW (object);

  switch (prop_id)
    {
    case PROP_SHOW_PREVIEWS:
      gtk_revealer_set_reveal_child (self->revealer, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
code_window_class_init (CodeWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = code_window_finalize;
  object_class->get_property = code_window_get_property;
  object_class->set_property = code_window_set_property;

  properties [PROP_SHOW_PREVIEWS] =
    g_param_spec_boolean ("show-previews", NULL, NULL,
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/ui/code-window.ui");
  gtk_widget_class_bind_template_child (widget_class, CodeWindow, buffer);
  gtk_widget_class_bind_template_child (widget_class, CodeWindow, entry);
  gtk_widget_class_bind_template_child (widget_class, CodeWindow, indexes);
  gtk_widget_class_bind_template_child (widget_class, CodeWindow, revealer);
  gtk_widget_class_bind_template_child (widget_class, CodeWindow, selection);
  gtk_widget_class_bind_template_child (widget_class, CodeWindow, title);
  gtk_widget_class_bind_template_child (widget_class, CodeWindow, view);
  gtk_widget_class_bind_template_callback (widget_class, on_search_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_selection_changed_cb);

  gtk_widget_class_install_property_action (widget_class, "show-previews", "show-previews");
}

static void
code_window_init (CodeWindow *self)
{
  GtkSourceStyleSchemeManager *sm;
  GtkSourceStyleScheme *scheme;

  sm = gtk_source_style_scheme_manager_get_default ();
  scheme = gtk_source_style_scheme_manager_get_scheme (sm, "Adwaita-dark");

  gtk_widget_init_template (GTK_WIDGET (self));

  gtk_source_buffer_set_style_scheme (self->buffer, scheme);

  gtk_list_box_set_header_func (self->indexes, indexes_header_func, NULL, NULL);
}

CodeWindow *
code_window_new (void)
{
  return g_object_new (CODE_TYPE_WINDOW, NULL);
}

void
code_window_add_index (CodeWindow *self,
                       CodeIndex  *index,
                       const char *title,
                       const char *icon_name)
{
  GtkListBoxRow *row;
  GtkImage *image;
  GtkLabel *label;
  GtkBox *box;

  g_return_if_fail (CODE_IS_WINDOW (self));
  g_return_if_fail (index != NULL);

  box = g_object_new (GTK_TYPE_BOX,
                      "orientation", GTK_ORIENTATION_HORIZONTAL,
                      "spacing", 6,
                      NULL);
  row = g_object_new (GTK_TYPE_LIST_BOX_ROW,
                      "child", box,
                      NULL);
  image = g_object_new (GTK_TYPE_IMAGE,
                        "icon-name", icon_name,
                        NULL);
  label = g_object_new (GTK_TYPE_LABEL,
                        "ellipsize", PANGO_ELLIPSIZE_END,
                        "label", title,
                        "xalign", .0f,
                        NULL);
  gtk_box_append (box, GTK_WIDGET (image));
  gtk_box_append (box, GTK_WIDGET (label));

  g_object_set_data_full (G_OBJECT (row),
                          "CODE_INDEX",
                          code_index_ref (index),
                          (GDestroyNotify)code_index_unref);

  gtk_list_box_append (self->indexes, GTK_WIDGET (row));
}
