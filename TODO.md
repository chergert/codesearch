 * We know that bsearching the trigram array isn't really worth it, it often
   ends up slower.  Since sequential access through the iter is fine, we
   should be able to do delta encoding on the integers and integer compression
   to save a lot of space.
 * It might make sense to encode a bit of extra information with trigram
   positions, such as if they start or end a line.  With that information we
   can resolve `^gtk_widget_` from the index only.
